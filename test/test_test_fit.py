import numpy as np
from emasses import EmassCalculator
from emasses.calculator import Calculator


def polynomial(r):
    return second_order_poly(r) + third_order_poly(r)


def second_order_poly(r):
    x = r[:, 0]
    y = r[:, 1]
    z = r[:, 2]
    f0 = 2
    f2 = (x**2 + y**2 + 1 / 2 * z**2) / 2
    return f0 + f2


def third_order_poly(r):
    x = r[:, 0]
    y = r[:, 1]
    z = r[:, 2]
    f3 = (1 / 10 * x**3 + 1 / 2 * y**3 - 1 / 3 * z**3) / 6
    return f3


class Calc(Calculator):

    def __init__(self):
        pass

    def calculate(self, k):
        return polynomial(k)


def test_fitted_polynomial():
    calc = Calc()
    EMC = EmassCalculator(calc)
    dr = 0.001
    fit = EMC.fit_polynomial(r0=np.array([0, 0, 0]), dr=dr)
    correct_Hessian = np.diag([1, 1, 1 / 2])
    assert np.linalg.norm(fit.Hessian - correct_Hessian) < dr
    N = 101
    r = EMC.linspace3d([- 0.5, - 0.7, - 0.1], [0.8, 1.1, 2.2], N)
    f2_fit = fit(r)
    error = f2_fit - second_order_poly(r)
    assert np.max(abs(error)) < dr
