import numpy as np
from emasses.emasses import EmassCalculator
from emasses.calculator import Calculator


class Calc(Calculator):

    def __init__(self, parabolicity_radius=0.001):
        self.parabolicity_radius = parabolicity_radius

    def calculate(self, x):
        if len(x.shape) == 1:
            x = x.reshape(1, -1)
        norm = np.linalg.norm(x, axis=1)
        f = norm**2
        non_parabolic_idx = np.argwhere(norm > self.parabolicity_radius)
        f[non_parabolic_idx] = np.exp(
            1 / (0.1j + norm[non_parabolic_idx])).real
        return f


def test_zoom_3d():
    calc = Calc()
    EMC = EmassCalculator(calc)
    out = EMC.zoom_and_fit(r0=np.array([0, 0, 0]), dr0=0.05,
                           zoom_factor=1 / 2)
    fit = out['fit']
    num_zooms_required = int(np.ceil(np.log(0.05 / 0.001) / np.log(2)))
    assert num_zooms_required == out['n_zooms']

    assert np.allclose(np.trace(fit.Hessian), 6)

    assert np.allclose(fit.gradient, np.zeros(3))


def test_zoom_2d():
    calc = Calc()
    EMC = EmassCalculator(calc)
    zoom_factor = 1 / np.sqrt(2)
    num_zooms_required = int(np.ceil(np.log(0.02 / 0.001)
                                     / np.log(1 / zoom_factor)))
    out = EMC.zoom_and_fit(r0=np.array([0, 0]), dr0=0.02,
                           zoom_factor=zoom_factor,
                           max_zoom_iterations=num_zooms_required + 5,
                           hessian_threshold=0.0001)
    fit = out['fit']
    assert num_zooms_required == out['n_zooms']
    assert np.allclose(np.trace(fit.Hessian), 4)
    assert np.allclose(fit.gradient, np.zeros(2))
