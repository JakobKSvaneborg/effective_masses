import numpy as np
from emasses import EmassCalculator


class Calculator():

    def __init__(self):
        pass

    def calculate(self, k):
        if len(k.shape) == 1:
            return np.linalg.norm(k)**2 / 2
        return np.linalg.norm(k, axis=1)**2 / 2


def test_3d():
    calc = Calculator()
    EMC = EmassCalculator(calc)
    x0 = EMC.find_extremum(x0=np.array([0.1, 0.2, 0]))
    # x0 = x0.reshape(1, -1)
    assert np.linalg.norm(x0) < 1e-9, 'did not find right minimum!'

    fit = EMC.fit_polynomial(r0=x0)

    Hessian = fit.Hessian

    assert abs(np.trace(Hessian) - 3) < 1e-10, 'wrong hessian!'


def test_2d():
    calc = Calculator()
    EMC = EmassCalculator(calc)
    x0 = EMC.find_extremum(x0=np.array([0.1, 0.2]))
    assert np.linalg.norm(x0) < 1e-9, 'did not find right minimum!'

    fit = EMC.fit_polynomial(r0=x0)

    Hessian = fit.Hessian

    assert abs(np.trace(Hessian) - 2) < 1e-10, 'wrong hessian!'
