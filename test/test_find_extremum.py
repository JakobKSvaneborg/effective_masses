import numpy as np
from scipy.optimize import rosen as rosen_scipy
from emasses import EmassCalculator


def rosen_2d(r, a, b):
    x = r[0]
    y = r[1]
    return (a - x)**2 + b * (y - x**2)**2


class Calculator():

    def __init__(self, a, b):
        self.a = a
        self.b = b

    def calculate(self, k):
        return rosen_2d(k, self.a, self.b)


class ScipyCalc():

    def calculate(self, k):
        return rosen_scipy(k)


def test_find_extremum():
    a = 1
    b = 100
    calc = Calculator(a, b)
    EMC = EmassCalculator(calc)
    x0 = np.array([0.2, 0.3])

    x_min = EMC.find_extremum(x0=x0)
    assert np.allclose(x_min, np.array([a, a**2]))

    a2 = 2.15
    b2 = 100
    calc2 = Calculator(a2, b2)
    EMC = EmassCalculator(calc2)
    x_min2 = EMC.find_extremum(x0=-x0)
    assert np.allclose(x_min2, np.array([a2, a2**2]))

    calc3 = ScipyCalc()
    EMC = EmassCalculator(calc3)
    x_min3 = EMC.find_extremum(x0=[-13.4, 10.0, 100.0])
    assert np.allclose(x_min3, np.array([1, 1, 1]))
