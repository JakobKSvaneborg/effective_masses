import numpy as np
from emasses import EmassCalculator


class Calculator():

    def __init__(self, func):
        self.func = func

    def calculate(self, k):
        return self.func(k)


def to_polar(r):
    x = r[:, 0]
    y = r[:, 1]

    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return rho, phi


def analytic_func(r):
    rho, phi = to_polar(r)

    return rho**2 / 2 * (1 + np.cos(2 * phi))


def warped_func(r):
    rho, phi = to_polar(r)

    return rho**2 / 2 * (1 + np.cos(4 * phi))


def non_parabolic(r):
    rho, phi = to_polar(r)
    return rho**2 / 2 * (1 + np.cos(2 * phi)) + 1 / 6 * rho**3 * np.cos(phi)


def warped_nonparabolic(r):
    rho, phi = to_polar(r)
    return rho**2 / 2 * (1 + np.cos(4 * phi)) + 1 / 6 * rho**3 * np.cos(phi)


def test_get_iems_analytic():
    calc = Calculator(analytic_func)
    EMC = EmassCalculator(calc)
    dr = 0.1
    fit = EMC.fit_polynomial(r0=np.array([[0, 0]]), dr=dr)
    iems_dict = EMC.get_iems_2d(fit)
    w = iems_dict['warping']
    assert np.allclose(w, 0)
    iems_coefficients_ks = iems_dict['coefficients_ks']
    assert np.allclose(iems_coefficients_ks[:, 1], 0)
    r1 = dr / 2
    r2 = dr
    conversion_matrix = np.array([[r1**2, r1**3],
                                  [r2**2, r2**3]])
    energies_ks = iems_coefficients_ks @ (conversion_matrix.T)\
        + iems_dict['f0']
    energies_k = energies_ks.reshape(-1, order='F')
    assert np.allclose(energies_k, analytic_func(iems_dict['kpts_circles_k']))


def test_get_iems_non_parabolic():
    calc = Calculator(non_parabolic)
    EMC = EmassCalculator(calc)
    dr = 0.1
    fit = EMC.fit_polynomial(r0=np.array([[0, 0]]), dr=dr)
    iems_dict = EMC.get_iems_2d(fit)
    w = iems_dict['warping']
    assert np.allclose(w, 0)
    iems_coefficients_ks = iems_dict['coefficients_ks']
    N = len(iems_dict['phi'])
    r1 = 2 * dr
    r2 = 5 * dr
    conversion_matrix = np.array([[r1**2, r1**3],
                                  [r2**2, r2**3]])
    energies_ks = iems_coefficients_ks @ (conversion_matrix.T)\
        + iems_dict['f0']

    k_r1 = iems_dict['kpts_circles_k'][N:] * r1 / dr
    k_r2 = k_r1 * r2 / r1

    assert np.allclose(energies_ks[:, 0], non_parabolic(k_r1))
    assert np.allclose(energies_ks[:, 1], non_parabolic(k_r2))


def test_get_iems_warped():
    calc = Calculator(warped_func)
    EMC = EmassCalculator(calc)
    dr = 0.1
    fit = EMC.fit_polynomial(r0=np.array([[0, 0]]), dr=dr)
    iems_dict = EMC.get_iems_2d(fit)
    w = iems_dict['warping']
    assert np.allclose(w, 1 / np.sqrt(2))
    iems_coefficients_ks = iems_dict['coefficients_ks']
    assert np.allclose(iems_coefficients_ks[:, 1], 0)
    N = len(iems_dict['phi'])
    r1 = 11 * dr
    r2 = 0.35 * dr
    conversion_matrix = np.array([[r1**2, r1**3],
                                  [r2**2, r2**3]])
    energies_ks = iems_coefficients_ks @ (conversion_matrix.T)\
        + iems_dict['f0']

    k_r1 = iems_dict['kpts_circles_k'][N:] * r1 / dr
    k_r2 = k_r1 * r2 / r1

    assert np.allclose(energies_ks[:, 0], warped_func(k_r1))
    assert np.allclose(energies_ks[:, 1], warped_func(k_r2))


def test_get_iems_warped_non_parabolic():
    calc = Calculator(warped_nonparabolic)
    EMC = EmassCalculator(calc)
    dr = 0.1
    fit = EMC.fit_polynomial(r0=np.array([[0, 0]]), dr=dr)
    iems_dict = EMC.get_iems_2d(fit)
    w = iems_dict['warping']
    assert np.allclose(w, 1 / np.sqrt(2))
    iems_coefficients_ks = iems_dict['coefficients_ks']
    N = len(iems_dict['phi'])
    r1 = 2.31 * dr
    r2 = 6.1 * dr
    conversion_matrix = np.array([[r1**2, r1**3],
                                  [r2**2, r2**3]])
    energies_ks = iems_coefficients_ks @ (conversion_matrix.T)\
        + iems_dict['f0']

    k_r1 = iems_dict['kpts_circles_k'][N:] * r1 / dr
    k_r2 = k_r1 * r2 / r1

    assert np.allclose(energies_ks[:, 0], warped_nonparabolic(k_r1))
    assert np.allclose(energies_ks[:, 1], warped_nonparabolic(k_r2))
