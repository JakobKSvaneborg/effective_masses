# flake8: noqa
"""
from ase.build import mx2
from gpaw import GPAW, FermiDirac
from gpaw.spinorbit import soc_eigenstates
import numpy as np
from emasses import find_extremum, zoom_and_fit, get_IEMS

slab = mx2(formula='MoS2', a=3.16, thickness=3.17, vacuum=5.0)
nbands = 15
try:
    calc = GPAW('gs_MoS2_lcao.gpw')
    slab.calc = calc
except FileNotFoundError:
    calc = GPAW(mode='lcao',
                xc='LDA',
                nbands=nbands,
                occupations=FermiDirac(width=0.05),
                kpts={'size': (5, 5, 1), 'gamma': True},
                txt='gs_MoS2.txt')

    slab = mx2(formula='MoS2', a=3.16, thickness=3.17, vacuum=5.0)
    slab.calc = calc
    slab.get_potential_energy()
    calc.write('gs_MoS2_lcao.gpw')

cbm_idx = 26
vbm_idx = 25

unit_cell = np.array(slab.cell)
reciprocal_cell = np.array(slab.cell.reciprocal())
K_point = (np.array([1 / 3, 1 / 3, 0]) @ reciprocal_cell)[:2]


def calculator(_kpt, band_idx):
    if len(_kpt.shape) == 1:
        kpt = _kpt.reshape(1, -1)
    else:
        kpt = _kpt.copy()
    if kpt.shape[-1] == 2:
        kpt = np.concatenate((kpt, np.zeros((len(kpt), 1))), axis=1)

    kpt_kbasis = kpt @ unit_cell.T
    calc_result = calc.fixed_density(
        symmetry='off',
        kpts=kpt_kbasis,
        occupations=FermiDirac(width=0.05),
        convergence={'bands': 13},
        txt=None)
    soc = soc_eigenstates(calc_result)
    SOC_energies_km = soc.eigenvalues()
    return SOC_energies_km[:, band_idx]


x0 = K_point - np.array([0.01, 0.05])
cbm = find_extremum(calculator, x0=x0, band_idx=cbm_idx, ftol=1e-4, xtol=1e-4)
print(cbm @ (unit_cell.T)[:2, :2])
vbm = find_extremum(calculator, extremum_type='max', x0=x0,
                    band_idx=vbm_idx, ftol=1e-4, xtol=1e-4)
print(vbm @ (unit_cell.T)[:2, :2])

assert np.linalg.norm(cbm - K_point) < 1e-3
assert np.linalg.norm(vbm - K_point) < 1e-3
"""