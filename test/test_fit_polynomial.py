import numpy as np
from emasses import EmassCalculator
from scipy.spatial.transform import Rotation


def polynomial_3d(r):
    if len(r.shape) == 1:
        r = (r.copy()).reshape(1, -1)
    x0 = np.array([[0.1, 0.17, -1.9]])
    Rot = Rotation.from_rotvec([-2.1598, 1.31, np.pi / 2])
    Q = Rot.as_matrix()
    H = Q @ np.diag([1, 3, 11]) @ (Q.T)

    fval = np.diag((r - x0) @ H @ (r - x0).T) / 2

    return fval


def polynomial_2d(r):
    if len(r.shape) == 1:
        r = (r.copy()).reshape(1, -1)
    grad = np.array([2, 3])
    hess = np.array([4, 1])
    fval = 5 + (r * grad + r**2 / 2 * hess).sum(1)
    return fval


class Calculator():

    def __init__(self, func):
        self.func = func

    def calculate(self, k):
        return self.func(k)


def test_3d():
    calc = Calculator(polynomial_3d)
    EMC_3d = EmassCalculator(calc)
    x0 = EMC_3d.find_extremum(x0=np.array([0, 0, 0]))
    assert np.allclose(x0, np.array([[0.1, 0.17, -1.9]])), 'minimum not found!'

    result = EMC_3d.fit_polynomial(r0=x0)
    eigs = np.linalg.eigvalsh(result.Hessian)
    assert np.allclose(eigs, np.array([1, 3, 11])), 'Hessian not correct!'


def test_2d():
    calc = Calculator(polynomial_2d)
    EMC_2d = EmassCalculator(calc)
    result_2d = EMC_2d.fit_polynomial(r0=np.array([0, 0]))
    assert np.allclose(result_2d.gradient, np.array([2, 3]))
    eigvals_2d = np.linalg.eigvalsh(result_2d.Hessian)
    assert np.allclose(eigvals_2d, np.array([1, 4]))
