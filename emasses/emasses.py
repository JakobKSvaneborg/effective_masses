# from ase import atoms
import numpy as np
from scipy.optimize import minimize as scipy_minimize
from ase.parallel import parprint
from emasses.calculator import Calculator


class FittedPolynomial:
    """Representation of a fitted 2nd order polynomial.
       This works as a helper class to keep track of variables.
       r0       : point at which fit is amde
       f0       : function value at r0
       gradient : gradient at r0
       Hessian  : Hessian at r0
       xvals    : xvalues used to make the fit
       fvals    : function values at xvals
       dr       : radius used for fit. e.g. gradient is computed from values
                  sampled at r0 + dr, r0 - dr.
    """

    def __init__(self, r0, f0, gradient, Hessian,
                 xvals=None, fvals=None, dr=None):
        self.r0 = np.asarray(r0).reshape(1, -1)
        self.f0 = f0
        self.gradient = gradient
        self.Hessian = Hessian
        self.xvals = xvals
        self.fvals = fvals
        self.dim = np.asarray(r0).size
        self.dr = dr
        self.eigvals, self.eigvecs = np.linalg.eigh(Hessian)

    def __call__(self, r):
        if len(np.asarray(r).shape) < 2:
            r_rel = np.asarray(r).reshape(1, -1) - self.r0
        else:
            r_rel = r - self.r0

        f0 = self.f0
        f1 = r_rel @ self.gradient
        f2 = (r_rel @ self.Hessian * r_rel).sum(1) / 2

        return f0 + f1 + f2


class EmassCalculator:

    def __init__(self, calculator: Calculator, verbose=True):
        self.calc = calculator
        self.verbose = verbose

    def get_emass(self, extremum_type: str, r0_approx: np.ndarray, dr=0.01,
                  optimize_r0: bool = True):
        self.dim = r0_approx.size
        if self.dim != 2 and self.dim != 3:
            raise ValueError('r0_approx must be 2- or 3-dimensional!'
                             ' Had size dim=%d' % self.dim)

        # First step is to find out the exact location in reciprocal space
        # where we have the CBM or VBM.
        #
        # Starting from r0_approx we maximize/minimize the energy of
        # the band (probably represented as an integer index, but that's up
        # to the emass calculator) we are working on.
        #
        # r0 will be the precise kpoint corresponding to CBM/VBM.
        if optimize_r0:
            r0 = self.find_extremum(x0=r0_approx, extremum_type=extremum_type)
        else:
            r0 = r0_approx
        # We make a sphere of radius dr around the miminum and sample a number
        # of band energies on that, which we fit.  If the fit is bad, we zoom
        # to smaller dr and redo, until the fit is good.
        zoom_result = self.zoom_and_fit(r0=r0, dr0=dr)
        fit = zoom_result['fit']
        if self.dim == 2:
            iems_dict = self.get_iems_2d(fit)
            self.iems_dict = iems_dict
            contour_dict = self.get_contour_2d(fit)
            self.contour_dict = contour_dict

    def find_extremum(self, x0=np.array([0.0, 0.0, 0.0]),
                      xtol=1e-8, ftol=1e-12, extremum_type='min',
                      bounds=None):
        """Find the k-point with the smallest/largest eigenvalue."""

        self.verboseprint('Running local optimization to find band '
                          + extremum_type + 'imum.')
        self.verboseprint('Start guess for extremum location: ', x0)
        if extremum_type not in {'min', 'max'}:
            raise ValueError('extremum_type should be \'min\' or \'max\'.'
                             f'Was \'{extremum_type}\'.')
        if extremum_type == 'min':
            def f(x):
                return self.calc.calculate(x)
        else:
            def f(x):
                return -self.calc.calculate(x)

        f0 = f(x0)
        opt_result_Nelder_Mead = scipy_minimize(fun=f,
                                                x0=x0, method='Nelder-Mead',
                                                options={'xatol': xtol,
                                                         'fatol': ftol},
                                                bounds=bounds)
        coords_Nelder_Mead = opt_result_Nelder_Mead.x
        fval_Nelder_Mead = opt_result_Nelder_Mead.fun
        opt_result_Powell = scipy_minimize(fun=f,
                                           x0=coords_Nelder_Mead,
                                           method='Powell',
                                           options={'xtol': xtol,
                                                    'ftol': ftol},
                                           bounds=bounds)
        coords_Powell = opt_result_Powell.x
        fval_Powell = opt_result_Powell.fun
        min_opt_result = min(fval_Powell, fval_Nelder_Mead)
        if fval_Powell <= fval_Nelder_Mead:
            coords = coords_Powell
        else:
            coords = coords_Nelder_Mead
        if f0 < min_opt_result:
            self.verboseprint('Initial guess for extremum was better than'
                              ' optimization result')
            coords = x0

        self.verboseprint('found extremum at r0 = ', coords)
        self.r0 = coords
        return coords

    def fit_polynomial(self, dr=0.03, r0=None, Q=None):

        if r0 is None:
            r0 = self.r0
        r0 = np.asarray(r0).copy().flatten()
        if Q is None:
            Q = np.eye(len(r0))

        def FD_points(dr, r0, Q):
            if len(r0) == 2:
                if (np.array(dr)).size == 2:
                    dx, dy = dr
                else:
                    dx = dr
                    dy = dr
                points = np.array([
                    [0, 0],
                    [dx, 0], [-dx, 0], [0, dy], [0, -dy],
                    [dx, dy], [dx, -dy], [-dx, dy], [-dx, -dy]
                ])
                return (points @ Q.T) + r0
            elif len(r0) == 3:
                if (np.array(dr)).size == 3:
                    dx, dy, dz = dr
                else:
                    dx = dr
                    dy = dr
                    dz = dr
                points = np.array([
                    [0, 0, 0],
                    [dx, 0, 0], [-dx, 0, 0], [0, dy, 0], [0, - dy, 0],
                    [0, 0, dz], [0, 0, -dz],
                    [0, dy, dz], [0, dy, -dz], [0, -dy, dz], [0, -dy, -dz],
                    [dx, 0, dz], [dx, 0, -dz], [-dx, 0, dz], [-dx, 0, -dz],
                    [dx, dy, 0], [dx, -dy, 0], [-dx, dy, 0], [-dx, -dy, 0]
                ])
                return (points @ Q.T) + r0

        kpts_k = FD_points(dr, r0, Q)
        fvals_k = self.calc.calculate(kpts_k)
        # calculates finite-difference gradient and Hessian from the array f,
        #  with points evaluated by the FD_points function

        if len(r0) == 2:
            dx = np.linalg.norm(kpts_k[1] - kpts_k[0])
            dy = np.linalg.norm(kpts_k[3] - kpts_k[0])
            f0 = fvals_k[0]
            grad = np.array([(fvals_k[1] - fvals_k[2]) / (2 * dx),
                             (fvals_k[3] - fvals_k[4]) / (2 * dy)
                             ])
            Hessian = np.array([
                [(fvals_k[1] + fvals_k[2] - 2 * fvals_k[0]) / (dx * dx),
                 (fvals_k[5] - fvals_k[6] - fvals_k[7]
                  + fvals_k[8]) / (4 * dx * dy), ],
                [(fvals_k[5] - fvals_k[6] - fvals_k[7]
                  + fvals_k[8]) / (4 * dx * dy),
                 (fvals_k[3] + fvals_k[4] - 2 * fvals_k[0]) / (dy * dy),
                 ],
            ])

        elif len(r0) == 3:
            dx = np.linalg.norm(kpts_k[1] - kpts_k[0])
            dy = np.linalg.norm(kpts_k[3] - kpts_k[0])
            dz = np.linalg.norm(kpts_k[5] - kpts_k[0])
            f0 = fvals_k[0]
            grad = np.array([(fvals_k[1] - fvals_k[2]) / (2 * dx),
                             (fvals_k[3] - fvals_k[4]) / (2 * dy),
                             (fvals_k[5] - fvals_k[6]) / (2 * dz),
                             ])
            Hessian = np.array([
                [(fvals_k[1] + fvals_k[2] - 2 * fvals_k[0]) / (dx * dx),
                 (fvals_k[15] - fvals_k[16]
                  - fvals_k[17] + fvals_k[18]) / (4 * dx * dy),
                 (fvals_k[11] - fvals_k[12]
                  - fvals_k[13] + fvals_k[14]) / (4 * dx * dz)
                 ],
                [(fvals_k[15] - fvals_k[16] - fvals_k[17] + fvals_k[18])
                    / (4 * dx * dy),
                 (fvals_k[3] + fvals_k[4] - 2 * fvals_k[0]) / (dy * dy),
                 (fvals_k[7] - fvals_k[8] - fvals_k[9]
                  + fvals_k[10]) / (4 * dy * dz)
                 ],
                [(fvals_k[11] - fvals_k[12] - fvals_k[13] + fvals_k[14])
                    / (4 * dx * dz),
                 (fvals_k[7] - fvals_k[8] - fvals_k[9]
                  + fvals_k[10]) / (4 * dy * dz),
                 (fvals_k[5] + fvals_k[6] - 2 * fvals_k[0]) / (dz * dz)
                 ]])
        grad = grad.T
        Hessian = Hessian.T

        polynomial = FittedPolynomial(r0=r0, f0=f0, gradient=grad,
                                      Hessian=Hessian, xvals=kpts_k,
                                      fvals=fvals_k, dr=dr)
        return polynomial

    def zoom_and_fit(self, r0, dr0, zoom_factor=np.sqrt(1 / 2),
                     max_zoom_iterations=20, grad_threshold=0.01,
                     hessian_threshold=0.025):

        dr1 = dr0 * zoom_factor
        number_of_zooms = 0
        dim = r0.size
        if not (dim == 2 or dim == 3):
            raise NotImplementedError('Error: zoom_and_fit is only'
                                      ' implemented for'
                                      ' 2- or 3-dimensional inputs')
        while True:
            fit_at_r0 = self.fit_polynomial(dr=dr0, r0=r0)
            fit_at_r1 = self.fit_polynomial(dr=dr1, r0=r0)
            grad0 = fit_at_r0.gradient
            Hess0 = fit_at_r0.Hessian
            Hess1 = fit_at_r1.Hessian

            Hess0diag = np.diag(Hess0)
            Hess1diag = np.diag(Hess1)
            NormHess = np.sum((Hess0diag - Hess1diag)**2) / \
                np.sqrt(np.sum(Hess0diag**2) * np.sum(Hess1diag**2))
            normGrad = np.linalg.norm(grad0)
            if NormHess < hessian_threshold and normGrad < grad_threshold:
                break
            if number_of_zooms >= max_zoom_iterations:
                break
            dr0 = dr1
            dr1 = dr0 * zoom_factor
            number_of_zooms += 1

        # test that zoom is sufficient in a different direction
        # - rotate by random angle == 0.6 radians
        angle1 = 2 * np.pi / 5
        if dim == 3:
            Qtest = np.array([[np.cos(angle1), np.sin(angle1), 0],
                              [-np.sin(angle1), np.cos(angle1), 0], [0, 0, 1]])
        else:
            Qtest = np.array([[np.cos(angle1), np.sin(angle1)],
                              [-np.sin(angle1), np.cos(angle1)]])
        continue_zooming_0 = False
        while True:
            fit_at_r0_rotated = self.fit_polynomial(dr=dr0, r0=r0,
                                                    Q=Qtest)
            fit_at_r1_rotated = self.fit_polynomial(dr=dr1, r0=r0,
                                                    Q=Qtest)
            grad0Q = fit_at_r0_rotated.gradient
            Hess0Q = fit_at_r0_rotated.Hessian
            Hess1Q = fit_at_r1_rotated.Hessian

            Hess0diag = np.diag(Hess0Q)
            Hess1diag = np.diag(Hess1Q)
            NormHess = np.sum((Hess0diag - Hess1diag)**2) / \
                np.sqrt(np.sum(Hess0diag**2) * np.sum(Hess1diag**2))
            normGrad = np.linalg.norm(grad0Q)
            if NormHess < hessian_threshold and normGrad < grad_threshold:
                break
            self.verboseprint('NormHess %.3f' % NormHess, flush=True)
            if number_of_zooms >= max_zoom_iterations:
                break
            self.verboseprint('Too large discrepancy - zooming', flush=True)

            dr0 = dr1
            dr1 = dr0 * zoom_factor
            number_of_zooms += 1
            continue_zooming_0 = True

        # test that zoom is sufficient in a third direction
        continue_zooming_1 = False
        angle2 = 2 * np.pi / 7
        if dim == 3:
            Qtest2 = np.array([[np.cos(angle2), np.sin(angle2), 0],
                               [-np.sin(angle2), np.cos(angle2), 0],
                               [0, 0, 1]])
        else:
            Qtest2 = np.array([[np.cos(angle2), np.sin(angle2)],
                               [-np.sin(angle2), np.cos(angle2)]])
        while True:

            fit_at_r0_rotated_2 = self.fit_polynomial(dr=dr0,
                                                      r0=r0, Q=Qtest2)
            fit_at_r1_rotated_2 = self.fit_polynomial(dr=dr1,
                                                      r0=r0, Q=Qtest2)
            grad0Q2 = fit_at_r0_rotated_2.gradient
            Hess0Q2 = fit_at_r0_rotated_2.Hessian
            Hess1Q2 = fit_at_r1_rotated_2.Hessian

            Hess0diag = np.diag(Hess0Q2)
            Hess1diag = np.diag(Hess1Q2)
            NormHess = np.sum((Hess0diag - Hess1diag)**2) / \
                np.sqrt(np.sum(Hess0diag**2) * np.sum(Hess1diag**2))
            normGrad = np.linalg.norm(grad0Q2)
            if NormHess < hessian_threshold and normGrad < grad_threshold:
                break
            self.verboseprint('NormHess %.3f' % NormHess, flush=True)
            if number_of_zooms >= max_zoom_iterations:
                break
            self.verboseprint('Too large discrepancy - zooming', flush=True)

            dr0 = dr1
            dr1 = dr0 * zoom_factor
            number_of_zooms += 1
            continue_zooming_1 = True

            # re-calculate polynomial in first direction based on current zoom
            if continue_zooming_0 or continue_zooming_1:
                fit_at_r0 = self.fit_polynomial(dr=dr0, r0=r0)

        self.fit = fit_at_r0
        self.n_zooms = number_of_zooms

        out = {}
        out['fit'] = fit_at_r0
        out['n_zooms'] = number_of_zooms
        return out

    @staticmethod
    def linspace3d(a, b, n):
        a = np.asarray(a)
        b = np.asarray(b)
        if a.size == 1:  # generate points in x-direction only
            assert b.size == 1
            x = np.linspace(a, b, n).reshape(-1, 1)
            y = np.zeros(x.shape)
            z = np.zeros(x.shape)
        else:
            ax, ay, az = a
            bx, by, bz = b

            x = np.linspace(ax, bx, n).reshape(-1, 1)
            y = np.linspace(ay, by, n).reshape(-1, 1)
            z = np.linspace(az, bz, n).reshape(-1, 1)
        return np.concatenate((x, y, z), axis=1)

    def get_iems_2d(self, fit, N=40, dr=None):
        # -------- construct circles (cbm) -----------
        N_input = N
        N = int(np.ceil(N / 4) * 4)
        if N_input != N:
            parprint('Warning: N should be divisble by 4. Changed\
             N from input %d -> %d' % (N_input, N))
        if dr is None:
            if fit.dr is None:
                raise ValueError('dr must be specified!')
            dr = fit.dr
        inner_radius = dr / 2
        r1 = inner_radius
        r2 = 2 * inner_radius
        dphi = 2 * np.pi / N
        phi = np.arange(0, 2 * np.pi, dphi)
        x1 = r1 * np.cos(phi).reshape(-1, 1)
        y1 = r1 * np.sin(phi).reshape(-1, 1)
        x2 = x1 * r2 / r1
        y2 = y1 * r2 / r1
        x = np.concatenate((x1, x2), axis=0)
        y = np.concatenate((y1, y2), axis=0)

        kpts_circles_k = fit.r0 + \
            x * np.array([[1, 0]]) + y * np.array([[0, 1]])
        kpts_circles_k = np.concatenate((fit.r0, kpts_circles_k))

        energies_k = self.calc.calculate(kpts_circles_k)
        f0 = energies_k[0]
        energies_k = energies_k[1:] - f0

        conversion_matrix = np.array([[r1**2, r1**3],
                                      [r2**2, r2**3]])
        inv_conversion_matrix = np.linalg.inv(conversion_matrix)

        energies_ks = energies_k.reshape(-1, 2, order='F')
        iems_coefficients_ks = energies_ks @ (inv_conversion_matrix.T)

        # calculate MDOS
        # IEMS has same sign in all directions, i.e. no saddle point
        if not np.any(np.diff(np.sign(iems_coefficients_ks[:, 0]))):
            m_dos = 1 / (2 * np.pi) \
                * np.sum(1 / abs(iems_coefficients_ks[:, 0])) * dphi
        else:
            m_dos = np.inf

        # calc warping

        hessian_trace_k = energies_ks[:, 0]\
            + np.roll(energies_ks[:, 0], int(N / 4))\
            + np.roll(energies_ks[:, 0], int(N / 2))\
            + np.roll(energies_ks[:, 0], int(3 * N / 4))

        warping = np.std(hessian_trace_k)\
            / np.mean(hessian_trace_k)

        out = {}
        out['warping'] = warping
        out['kpts_circles_k'] = kpts_circles_k[1:]
        out['phi'] = phi
        out['coefficients_ks'] = iems_coefficients_ks
        out['energies_ks'] = energies_ks + f0
        out['r0'] = fit.r0
        out['f0'] = f0
        out['m_dos'] = m_dos
        out['r1'] = r1
        out['r2'] = r2

        return out

    def get_contour_2d(self, fit, side_length=None, density=800, N_min=10,
                       N_max=35):

        if side_length is None:
            side_length = 5 * fit.dr0
        Nx = int(np.ceil(density * side_length))
        self.verboseprint('Nx determined from density: ', Nx)
        self.verboseprint('Nx must be between %d and %d. If the above is \
            outside that range, it will be changed.' % (N_min, N_max))
        Nx = max(Nx, 10)
        Nx = min(Nx, 35)
        Ny = Nx
        xSurf = np.linspace(-side_length / 2, side_length / 2, Nx)
        ySurf = np.linspace(-side_length / 2, side_length / 2, Ny)
        X, Y = np.meshgrid(xSurf, ySurf)
        X = X.reshape(-1, 1)
        Y = Y.reshape(-1, 1)
        surface_kpts = np.concatenate((X, Y), axis=1)

        surface_energies = self.calc.calculate(
            surface_kpts + fit.r0)

        X = X.reshape(Ny, Nx)
        Y = Y.reshape(Ny, Nx)

        Z = surface_energies.reshape(X.shape)

        out = {}
        out['contour_kx'] = X
        out['contour_ky'] = Y
        out['contour_energies'] = Z

        return out

    @staticmethod
    def find_array_neighbors(M, idx):
        neighbors = []
        if len(M.shape) == 1:
            if idx > 0:
                neighbors.append(idx - 1)
            if idx < M.size - 1:
                neighbors.append(idx + 1)
            return neighbors
        # 2d array
        assert len(M.shape) == 2
        if hasattr(idx, '__iter__'):
            x, y = idx
        else:  # idx is an integer, not tuple
            x = idx // M.shape[-1]
            y = idx % M.shape[-1]
        potential_neighbor_list = [(x - 1, y - 1), (x, y - 1), (x + 1, y - 1),
                                   (x - 1, y), (x + 1, y), (x - 1, y + 1),
                                   (x, y + 1), (x + 1, y + 1)]
        for potential_neighbor in potential_neighbor_list:
            try:
                assert potential_neighbor[0] >= 0
                assert potential_neighbor[1] >= 0
                M[potential_neighbor]
                neighbors.append(potential_neighbor)
            except:
                pass
        return neighbors

    def find_barrier(self, R, energies, levels=np.linspace(0, 0.02, 100),
                     R_max=None):
        Emin = energies.min()
        xmin = np.argmin(energies)
        max_R_flooded = [0.0]
        next_level = [0.0]
        max_R_flooded_idx = [xmin]
        is_flooded_bool_array = np.zeros(R.shape, dtype=bool)
        R_argsorted = np.argsort(R.flatten())
        if len(R.shape) > 1:
            is_flooded_bool_array[xmin // R.shape[-1],
                                  xmin % R.shape[-1]] = True
        else:
            is_flooded_bool_array[xmin] = True
        IsFlooded_list = [is_flooded_bool_array.copy()]
        for level in levels:
            under_current_level_idx = np.where(
                energies.flatten() <= Emin + level)[0]
            while True:
                updated = False
                for index in under_current_level_idx:
                    indexTuple = (index // R.shape[-1], index % R.shape[-1])
                    neighbors = self.find_array_neighbors(
                        is_flooded_bool_array, index)
                    for neighbor in neighbors:
                        if is_flooded_bool_array[neighbor]:
                            if not is_flooded_bool_array[indexTuple]:
                                updated = True
                                is_flooded_bool_array[indexTuple] = True
                if not updated:
                    break
            if np.max(R[is_flooded_bool_array]) != max_R_flooded[-1]:
                max_R_flooded.append(np.max(R[is_flooded_bool_array]))
                next_level.append(level)
                max_R_flooded_idx.append(
                    R_argsorted[np.where(is_flooded_bool_array.flatten()
                                         [R_argsorted])[0][-1]])
                IsFlooded_list.append(is_flooded_bool_array.copy())
            if R_max is not None and max_R_flooded[-1] >= R_max:
                break

        out = {}
        out['R'] = max_R_flooded
        out['R_idx'] = max_R_flooded_idx
        out['levels'] = next_level
        out['Flooded_list'] = IsFlooded_list
        out['Flooded'] = is_flooded_bool_array
        return out

    def test_fit_along_lines(self, fit, lineLength=0, N=51):
        if lineLength == 0:
            lineLength = 6 * fit.dr

        kpts2d_x = self.linspace3d([-lineLength / 2, 0, 0],
                                   [lineLength / 2, 0.0, 0], 51)[:, :2]
        kpts2d_y = self.linspace3d([0, -lineLength / 2, 0],
                                   [0, lineLength / 2, 0], 51)[:, :2]
        if fit.dim != 2:
            raise NotImplementedError('test_fit_along_lines currently only'
                                      ' implemented for 2D materials')
        r0 = fit.r0
        Hessian = fit.Hessian

        # Test fit along principal axes
        E0, Q0 = np.linalg.eigh(Hessian)
        kpts_principal_axis_0 = r0 + kpts2d_x @ Q0.T
        kpts_principal_axis_1 = r0 + kpts2d_y @ Q0.T
        kpts_between_principals = (
            kpts_principal_axis_0 + kpts_principal_axis_1 - 2 * r0)\
            / np.sqrt(2) + r0

        fit_principal_0 = fit(kpts_principal_axis_0)
        fit_principal_1 = fit(kpts_principal_axis_1)
        fit_between_principals = fit(kpts_between_principals)

        func_principal_0 = self.calc.calculate(
            kpts_principal_axis_0)
        func_principal_1 = self.calc.calculate(
            kpts_principal_axis_1)
        func_between_prinicpals = self.calc.calculate(
            kpts_between_principals)

        out = {}
        out['KS_energies_max_emass'] = func_principal_0
        out['KS_energies_min_emass'] = func_principal_1
        out['KS_energies_mid_emass'] = func_between_prinicpals
        out['fit_max_emass'] = fit_principal_0
        out['fit_min_emass'] = fit_principal_1
        out['fit_mid_emass'] = fit_between_principals
        out['kpts_max_emass'] = kpts_principal_axis_0
        out['kpts_min_emass'] = kpts_principal_axis_1
        out['kpts_mid_emass'] = kpts_between_principals

        return out

    def verboseprint(self, *args, **kwargs):
        if self.verbose:
            parprint(*args, **kwargs)
