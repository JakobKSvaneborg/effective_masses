from abc import ABC, abstractmethod


class Calculator(ABC):

    @abstractmethod
    def calculate(self, kpt_kv):
        # calculate electronic band structure at specified k-points
        raise NotImplementedError()
