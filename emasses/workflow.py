from mq.workflow import run
from emasses.emass_recipe import main
from asr.core import read_json
from pathlib import Path


def workflow():
    if Path('results-asr.gs.json').is_file():
        gsresults = read_json("results-asr.gs.json")
        gap = gsresults.get("gap")
        gap_tolerance = 0.01
        if gap > gap_tolerance:
            run(function=emass, cores=24, tmax='2d')


def emass():
    main(gspath='gs.gpw')
